package my.realtime.realtimetesting.controllers;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import my.realtime.realtimetesting.models.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class SocketController {

    private SocketIONamespace socketIONamespace;

    public SocketIONamespace getSocketIONamespace(){
        return socketIONamespace;
    }

    //call socket server
    // and create listener event
    @Autowired
    public SocketController(SocketIOServer server){
        System.out.println("Server was add namespace /action");
        this.socketIONamespace=server.addNamespace("/action");
        //when client emit it mean client want to tranfer action on event /chat
        System.out.println("Server was create event /chat");
        this.socketIONamespace.addEventListener("chat",Price.class,clientAction);
    }

    public DataListener<Price> clientAction=new DataListener<Price>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Price price, AckRequest ackRequest) throws Exception {
            System.out.println("Client Listened Event /Chat......."+price);
            // client get event and broadcast more to other client
            System.out.println("Server Listen event /onbid from clients");
            socketIONamespace.getBroadcastOperations().sendEvent("bid",socketIOClient,price);
        }
    };

    public void broadcastEvent(String event, Object data){
        this.getSocketIONamespace().getBroadcastOperations().sendEvent(event,data);
    }

    public void stopServer() {
        this.stopServer();
    }
}
