package my.realtime.realtimetesting.controllers;

import my.realtime.realtimetesting.models.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {

    @Autowired
    private SocketController socketController;

    private Price price=new Price();

    @GetMapping({"/","/bid"})
    public String index(Model model){
        model.addAttribute("price",price);
        return "index";
    }



    @PostMapping("/bid")
    public String submit(Model model,@ModelAttribute("price") Price price){
        System.out.println("=> Controller Loaded...."+price.getPrice());
        int total=price.getPrice();
        price.setPrice(total);
        System.out.println("Price = "+price.getPrice());
        socketController.broadcastEvent("bid",price);
        model.addAttribute("price",price);
        return "index";
    }

}
