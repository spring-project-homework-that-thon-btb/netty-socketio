package my.realtime.realtimetesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealtimeTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(RealtimeTestingApplication.class, args);
    }

}

