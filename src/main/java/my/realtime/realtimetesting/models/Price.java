package my.realtime.realtimetesting.models;

public class Price {

    private Integer price;

    public Price() {
    }

    public Price(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
